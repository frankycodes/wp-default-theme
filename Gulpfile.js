var gulp = require( 'gulp' ),
  autoprefixer = require( 'gulp-autoprefixer' ),
  cssnano = require( 'gulp-cssnano' ),
  sass = require( 'gulp-sass' ),
  scsslint = require( 'gulp-sass-lint' ),
  plumber = require( 'gulp-plumber' ),
  rename = require( 'gulp-rename' ),
  uglify = require( 'gulp-uglify' ),
  eslint = require( 'gulp-eslint' ),
  concat = require( 'gulp-concat' ),
  watch = require( 'gulp-watch' ),
  browserSync = require( 'browser-sync' ).create(),
  reload = browserSync.reload,
  phplint = require( 'gulp-phplint' );
const babel = require('gulp-babel');

gulp.task( 'sass', function () {
  return gulp.src( 'wp-content/themes/base-theme/styles/app.scss' )
    .pipe( plumber() )
    .pipe( scsslint() )
    .pipe( autoprefixer( {
      browsers: ['last 2 versions']
    } ) )
    .pipe( sass().on( 'error', sass.logError))
    .pipe( rename( {
      basename: 'style',
    } ) )
    .pipe( gulp.dest( 'wp-content/themes/base-theme/' ) )
    .pipe( rename( {
      suffix: '.min',
    } ) )
    .pipe( cssnano() )
    .pipe( gulp.dest( 'wp-content/themes/base-theme/dist' ) );
} );

gulp.task( 'scripts', function () {
  return gulp.src( ['wp-content/themes/base-theme/js/**/*.js', '!node_modules/**'])
    .pipe( eslint({fix: true}) )
    .pipe( eslint.format() )
    .pipe(babel({
      presets: ['es2015'],
    }))
    .pipe( concat( 'scripts.js' ) )
    .pipe( gulp.dest( 'wp-content/themes/base-theme/dist' ) )
    .pipe( rename( 'scripts.min.js') )
    .pipe( uglify())
    .pipe( gulp.dest( 'wp-content/themes/base-theme/dist' ) );
} );

gulp.task( 'phplint', function () {
  return gulp.src( ['wp-content/themes/base-theme/**/*.php'] )
  .pipe( phplint() );
} );

gulp.task( 'watch', function () {
  browserSync.init( {
    files: ['./**/*.php'],
    proxy: 'http://localhost:8888/',
    port: 3000
  } )
  gulp.watch( 'wp-content/themes/base-theme/styles/**/*.scss', ['sass', reload] );
  gulp.watch( 'wp-content/themes/base-theme/js/**/*.js', ['scripts', reload] );
  gulp.watch( 'wp-content/themes/base-theme/**/*.php', ['phplint', reload] );
} );

gulp.task( 'default', ['watch', 'scripts', 'sass', 'phplint'] )
