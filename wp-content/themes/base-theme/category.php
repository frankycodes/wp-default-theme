<?php get_header(); ?>

<section id="blog-post" class="box--filled-cooled u-pv++">
	<div class="container">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="row">
						<div class="small-12 medium-3 columns text-center u-mb">
							<div class="circle circle--large box--filled text-white text-center">
								<p class="u-mb0 zeta u-pt-- fw-bold"><?php the_time('j'); ?></p>
								<p class="u-mb0 zeta fw-bold"><?php the_time('M'); ?></p>
							</div>
						</div>
						<div class="small-12 medium-9 columns box--filled-white u-pv+ u-ph u-mb">
							<h2 class="fw-bold"><a class="link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							 <?php the_content('<div class="button button--primary u-mt">Read More</div>'); ?>
						</div>
					</div>
				<?php endwhile; else: ?>
				<p><?php _e('Sorry, there are no posts.'); ?></p>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns text-center">
				<?php echo wpbeginner_numeric_posts_nav(); ?>
				<a href="<?php echo get_home_url(); ?>/blog/" class="button button--secondary icon-left-open u-mb+"> Back to all posts</a>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>