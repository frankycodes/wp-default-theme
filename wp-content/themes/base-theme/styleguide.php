<?php
/**
 * Page Name: Style Guide
 *
 * @package WordPress
 * @subpackage
 * @since
 */
?>
<section>
  <nav>
    <ul>
      <li>Color</li>
      <li>Typography</li>
      <li>Buttons</li>
      <li>Forms</li>
    </ul>
  </nav>
</section>

<section>
  <div class="row">
    <div class="small-12 columns">
      <div class="u-flex">
        <div class="box">
          <p>color code</p>
        </div>
        <div>
          <p>color name</p>
        </div>
      </div>
    </div>
  </div>
</section>
<?php the_content(); ?>